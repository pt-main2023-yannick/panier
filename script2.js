var main = document.querySelector(".main");
main.style.display = "flex";
main.style.justifyContent = "space-around";
main.style.alignItems = "center";
main.style.paddingTop = "25px";
var image = document.querySelectorAll("img");
var plus = document.querySelectorAll("#plus");
var less = document.querySelectorAll("#less");
var quantity = document.querySelectorAll("#quantity");
var blocImages = document.querySelectorAll(".bloc_images");
var quantity = document.querySelectorAll('#quantity');
var total = document.querySelectorAll('#total');
var unit = document.querySelectorAll('#unit');
var titre = document.querySelectorAll("h1");
var likes = document.querySelectorAll("#like");


for (const key in likes) {
   if (likes && likes[key].addEventListener) {
    likes[key].addEventListener("click", (e)=>{
        if (likes[key].classList.contains("red-heart")) {
            likes[key].classList.remove("red-heart");
            likes[key].style.color = "black";
          } else {
            likes[key].classList.add("red-heart"); // Ajoute la classe "red-heart"
            if (likes[key].classList.contains("red-heart")) {
                likes[key].style.color = "red";
            }
          }
    })
   }
}

for (const key in plus) {
   if(plus && plus[key].addEventListener){
    plus[key].addEventListener("click", (e)=>{
        if ( quantity[key]) {
            var  quantityValue =  quantity[key].value;
            if (parseInt(quantityValue)  >= 0 ) {
                var qTotal =  quantity[key].value = parseInt(quantityValue) + 1;
                total[key].textContent = qTotal * parseInt(unit[key].textContent);
            }
        }
    })
   }
}

for (const key in less) {
    if(less && less[key].addEventListener){
        less[key].addEventListener("click", (e)=>{
            if ( quantity[key]) {
                var  quantityValue =  quantity[key].value;
                if (parseInt(quantityValue)  > 1 ) {
                    var qTotal =  quantity[key].value = parseInt(quantityValue) - 1;
                    total[key].textContent = qTotal * parseInt(unit[key].textContent);
                }
            }
        })
       }
}

var container = document.querySelectorAll(".container");
for (const key in container) {
    if (container[key]) {
        var containerData = container[key]
        var imageData = image[key];
        var plusData = plus[key];
        var lessData = less[key];
        var blocImagesData = blocImages[key];
        var quantityData = quantity[key];
        var titreData = titre[key];
        var likeData = likes[key];

        if (containerData && containerData.style) {
            containerData.style.position = "relative";
            containerData.style.width = "300px";
            containerData.style.borderRadius = "10px";
            containerData.style.overflow = "hidden";
            containerData.style.display = "flex"
            containerData.style.flexDirection = "column";
            containerData.style.justifyContent = "center";
            containerData.style.alignItems = "center";
            
            containerData.style.boxShadow = "0 2px 6px orangered";
            containerData.style.paddingBottom = "20px";

            
            imageData.style.width = "200px";
            imageData.style.height = "200px";

            plusData.style.backgroundColor = "green";
            plusData.style.border = "none";
            plusData.style.padding = "10px 20px";
            plusData.style.borderRadius = "10px";

            lessData.style.backgroundColor = "red";
            lessData.style.border = "none";
            lessData.style.padding = "10px 20px";
            lessData.style.borderRadius = "10px";

            quantityData.style.padding = "8px 0px";
            quantityData.style.textAlign = "center";

            titreData.style.color = "rgb(15, 15, 117)";
            titreData.style.textAlign = "center";
            titreData.style.fontSize = "25px";
            titreData.style.fontWeight = "bold";
            titreData.style.fontFamily = "Roboto";

            likeData.style.position = "absolute";
            likeData.style.right = "5px";
            likeData.style.top = "5px";
            likeData.style.fontSize = "30px";
            likeData.style.cursor = "pointer";
            
        }
        
    }
}